# Lufft_CHM15k_Check

Lufft CHM15k ceilometer status check and send email alerts.

**Set the ip address in**
_check_ceilometer.py_

`ip = 'ip_address'`

**Set parameters of the account and message in**
_smtp_send.py_

`mail_server = 'server_address: port'`

`password = "password"`

`user_name = "username"`

`from_mail = "user@mail.com"`

`to_mail = "email_1@mail.com,email_2@mail.com"`

`subject = "Ceilometer Alert"`

**Run**

`python3 check_ceilometer.py`

**Recommendations:** Add to crontab



_Author: Juan Carlos Antuña-Sanchez_

_Email: jcantuna@goa.uva.es_