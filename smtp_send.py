#
# author : Juan Carlos Antuña-Sanchez
# email : jcantuna@goa.uva.es
# date : 11/10/20
# -*- coding: utf-8
# ------------------------------------

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from datetime import datetime

# setup the parameters of the account and message
mail_server = 'server_address: port'
password = "password"
user_name = "username"
from_mail = "user@mail.com"
to_mail = "email_1@mail.com,email_2@mail.com"
subject = "Ceilometer Alert"

def send_alert_mail(status):
    # create message object instance
    msg = MIMEMultipart()

    # now datetime
    now = datetime.now()
    now_format = now.strftime("%Y-%m-%d %H:%M:%S")

    if status == "offline":
        message = "Ceilometer disconnected at " + str(now_format)
    else:
        message = now + "  " + status

    # Message and account settings.
    msg['From'] = from_mail
    msg['To'] = to_mail
    msg['Subject'] = subject

    # add in the message body
    msg.attach(MIMEText(message, 'plain'))

    # create server
    server = smtplib.SMTP(mail_server)

    server.starttls()

    # Login Credentials for sending the mail
    server.login(user_name, password)

    # send the message via the server.
    server.sendmail(msg['From'], msg['To'].split(","), msg.as_string())

    server.quit()